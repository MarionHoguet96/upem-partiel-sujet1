package fr.upem.partiel.part1

import java.time.Instant
import java.time.temporal.ChronoUnit.YEARS



// Part 1 (10pts)
object Part1 {

  // 1.1 Apply 'mul2' using pattern matching to the given integer (.5pts)
  def mul2(i: Int): Int = i * 2
  def applyMul2WithPatternMatching(i: Option[Int]): Option[Int] = {
    i match {
      case Some(x) => Some(x * 2);
      case _ => None
    }
  }

  // 1.2 Apply 'mul2' WITHOUT using pattern matching to the given integer (.5pts)
  def applyMul2WithoutPatternMatching(i: Option[Int]): Option[Int] = {
    if(i == None) None else Some(i.get * 2);
  }

  // 1.3 Refactor the following code using pattern matching (1pts)
  sealed trait Animal
  case object Cat extends Animal
  case object Bird extends Animal
  case class Dog(age: Int) extends Animal

  def formatAnimal(animal: Animal): String =
    animal match {
      case Cat => "It's a cat"
      case Bird => "It's a bird"
      case d if (d.isInstanceOf[Dog]) => s"It's a ${d.asInstanceOf[Dog].age} year old dog"
      case _ => "It's not an animal"
    }


  // 1.4 Find the index of the given element if any, use recursivity (1pts)
  def indexOf[A](l: List[A], a: A): Option[Int] = {
    l match {
      case h::t => if(h == a) Some(0) else indexOf(t,a) match { //on fait some(0) car les index commencent à 0
        case Some(x) => Some(1 + x)
        case _ => None
      }
      case h => if(h==a) Some(0) else None
    }
  }


  // 1.5 Throw away all errors (.5pts)
  case class Error(message: String)
  def keepValid[A](l: List[Either[Error, A]]): List[A] = {
    val (leftList, rightList) = l.partition(_.isLeft)
    rightList.map(_.right.get)
  }

  // 1.6 Aggregate values (.5pts)
  def aggregate[A](l: List[A], combine: (A, A) => A, empty: A): A = {
    l match {
      case Nil => empty
      case _ => l.reduce((x,y) => if(x == Nil) combine(empty,y) else if (y == Nil) combine(x,empty) else if(x == Nil && y == Nil) empty else combine(x,y))
    }
  }

  // 1.7 Aggregate valid values (.5pts)
  def aggregateValid[A](l: List[Either[Error, A]], combine: (A, A) => A, empty: A): A = {
    l match {
      case Nil => empty
      case _ => {
        val newList = keepValid(l)
        aggregate(newList, combine, empty)
      }
    }
  }


  // 1.8 Create the Monoid typeclass and rewrite the above "aggregateValid" (.5pts)
  trait Semigroup[A] {
    def combine(a: A, b: A) : A
  }

  trait Monoid[A] extends Semigroup[A]{
    def empty:A
  }

  // 1.9 Implement the Monoid typeclass for Strings and give an example usage with aggregateValidM (.5pts)
  def aggregateValidM{
    implicit val stringConcatMonoid: Monoid[String] = new Monoid[String] {
      def combine(a: String, b: String): String = a + " " + b

      def empty: String = ""
    }
  }


  // 1.10 Refactor the following object oriented hierarchy with an ADT (1.5pts)

  trait Earnings[T] {
    def computeEarnings(t:T): Double
  }

  sealed trait FinancialAsset{
    def computeEarnings: Double
  }

  case class FlatRateAsset(rate:Double, amount:Double)

  object FlatRateAsset{
    implicit val Earnings = new Earnings[FlatRateAsset] {
      def computeEarnings(flatRateAsset: FlatRateAsset)=  flatRateAsset.amount + (flatRateAsset.amount * flatRateAsset.rate)
    }
  }

  case class LivretA(amount:Double, Rate:Double = 0.75)


  case class Pel(rate:Double = 1.5,amount: Double, creation: Instant, GovernmentGrant:Int = 1525)

  object Pel{
    implicit val Earnings = new Earnings[Pel] {
      def computeEarnings(p:Pel): Double =
        if (Instant.now().minus(4, YEARS).isAfter(p.creation))
          computeEarnings(p) + p.GovernmentGrant
        else
          computeEarnings(p)
    }
  }

  case class CarSale(amount:Int, StateHorsePowerTaxation:Int = 500, horsePower:Int)
  object CarSale {
    implicit val Earnings = new Earnings[CarSale]{
      def computeEarnings(c:CarSale):Double = c.amount - (c.StateHorsePowerTaxation * c.horsePower)
    }
  }

  // 1.11 Extract the "computeEarnings" logic of the above hierarchy
  // into an "Earnings" typeclass and create the adequate instances (1.5pts)


  // 1.12 Rewrite the following function with your typeclass (.5pts)
  /*def computeTotalEarnings(assets: List[FinancialAsset]): Double =
    assets.map(_.computeEarnings).sum

  */
  def computeTotalEarnings[T](assets: List[T])(implicit ev: Earnings[T]) : Double = {
    assets.map(ev.computeEarnings(_)).sum
  }
  // 1.13 Enrich the "String" type with an "atoi" extension method that parses the
  // given String to an Int IF possible (1pts)

}
